# README #

* Summary of set up

in the local postgres instance create the following db under postgres user

CREATE DATABASE order
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_IN'
       LC_CTYPE = 'en_IN'
       CONNECTION LIMIT = -1;
	   
also create the following table 
create table order_lines(number integer not null, order_id varchar, item varchar, price integer, quantity integer, total integer, foreign key (order_id) references orders (order_id));

create table orders(order_id varchar primary key, date date , name varchar, total integer, address varchar);


* Dependencies
 postgres
 
 psycopg2 postgres driver (apt-get install psycopg2)

* starting :

 $ python order.py